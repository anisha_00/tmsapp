<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VerificationController;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');
// Route::get('userLogin',UserController::class)->name('');
Route::get('/register', function () { return view('user.register');});
Route::get('/login', function () { return view('user.user_login');});
Route::get('/admin', function () { return view('admin.admin_login');});

Route::get('/createTask', [AdminController::class,'createTask'])->name('createTask');
Route::post('createTask', [AdminController::class,'create'])->name('createTask');

Route::get('/manageTask', [AdminController::class,'manageTask'])->name('manageTask');
Route::get('/logout', [AdminController::class,'destroy'])->name('logout');


Route::get('/dashboard',  function () { return view('dashboard');});

Route::get('/editTask/{key}', [AdminController::class,'editTask'])->name('editTask');
Route::post('/editTask/{key}', [AdminController::class,'update'])->name('editTask');

Route::get('/deleteTask/{key}',[AdminController::class,'delete'])->name('deleteTask');

Route::get('adminLogin',[AdminController::class,'index'])->name('adminLogin');
Route::post('adminLogin',[AdminController::class,'store'])->name('adminLogin');
Route::get('adminDashboard',function (){return view('admin.adminDashboard');});

Route::get('userLogin',[UserController::class,'create'])->name('userLogin');
Route::post('userLogin',[UserController::class,'show'])->name('userLogin');
Route::get('register',[UserController::class,'index'])->name('register');
Route::post('register',[UserController::class,'store'])->name('register');
Route::get('/logout', [UserController::class,'destroy'])->name('logout');

Route::get('/updateUserTask/{key}', [UserController::class,'updateTask'])->name('updateUserTask');
Route::post('/updateUserTask/{key}', [UserController::class,'update'])->name('updateUserTask');

Route::get('/viewTask',[UserController::class,'edit'])->name('viewTask');
Route::get('/userBoard',  function () { return view('user.userBoard');});

Route::get('/verify/{token}', [VerificationController::class,'verify'])->name('verification.verify');
