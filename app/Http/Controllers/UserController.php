<?php

namespace App\Http\Controllers;

use App\Mail\mailverify;
use App\Models\emailverfication;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view("user.register");
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("user.user_login");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {$token=Str::random(5    );
        $data= $request->validate([
            'name'=> 'required',
            'email'=> 'required',
            'phoneNo'=> 'required',
            'password'=> 'required'

            ]);


       $user=user::create([
        'name'=> $data['name'],
        'email'=> $data['email'],
        'phoneNo'=> $data['phoneNo'],
        'password'=> Hash::make($data['password']),
        'status'=>false,
        'verification_token'=> $token,
       ]);
        $user->status=false;
       $verified=emailverfication::create([
            'user_id' => $user->id,
            'is_verified' => $user->status,
           'token'=>$token,
           'expired_at'=>Carbon::now()->addMinute()
        ]);

        Mail::to($user->email)->send(new mailverify($user));
        return view('welcome');
    }

    public function show(Request $request)
    {   $data= $request->validate([
        'name'=> 'required',
        'email'=> 'required',
        'password'=> 'required'
    ]);
        $user = user::where('email',$data['email'])->first();

        if($user) {
            $userPassword = $user->password;
            if (Hash::check($data['password'], $userPassword)) {
                if ($user->status == true) {
                    session()->put('id', $user->id);
                    session()->put('name', $user->name);
                    return redirect('userBoard')->with('status', 'You are logged in!!');
                }
                    return redirect()->back()->with('status', 'Error you are not verified!');
            }
            return redirect()->back()->with('status', 'Error could not logg in check password!');
        }

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit()
    {

        $userId = session()->get('id');
        $tasks = Task::where('user_id',$userId)->get();
        return view('user.task',compact('tasks'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $key)
    {
        $data = $request->validate([
            'status' => 'required',
        ]);
        $task = Task::find($key);
        $date=Carbon::today();
        if($task) {
            if()
            $task->status = $data['status'];
            $task->update();

            return redirect()->back()->with('status', 'Updated!');
        }
        return redirect()->back()->with('status', 'Error!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function updateTask(Request $request,$key){
        return view('user.updateUserTask')->with('key', $key);
    }
    public function destroy()
    {
        session()->forget('id');
        session()->forget('name');
        return redirect('/');
    }
}
