<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\emailverfication;
use App\Models\Task;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.admin_login');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $data = $request->validate([
            'email' => 'required',
            'description' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
        ]);
       $userid = user::where('email', $data['email'])->first();

       if ($userid) {
            $newUser = Task::create([
                'user_id' => $userid['id'],
                'description' => $data['description'],
                'startDate' => $data['startDate'],
                'endDate' => $data['endDate'],
                'status' => 'Not Started'
            ]);

           return redirect()->back()->with('status', 'Task Created!');
       } else {
            // Handle the case where the user with the specified email is not found
           return redirect()->back()->with('status', 'User not found with the specified email.');
       }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data=$request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        $admin=Admin::where('name',$data['name'])->first();
        if ($data['password']=$admin->password) {
            session()->put('id');
            session()->put('name');
            return redirect('dashboard')->with('status', 'Logged in!');
        }
        else{

        return redirect('adminLogin')->withErrors('Error could not Sign In!!');}
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return view('adminDashboard');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function createTask(){
        $users = user::where('status',true)->get();

        return view('admin.createTask',compact('users'));
    }
    public function manageTask(){
        $tasks = Task::all();

        return view('admin.manageTask',compact('tasks'));
    }
    public function editTask(Request $request,$key){
        return view('admin.editTask')->with('key', $key);
    }
    public function delete( $key){
        DB::delete('delete from tasks where id = ?',[$key]);
        echo "Record deleted successfully.<br/>";

        return redirect('manageTask');
    }
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,$key)
    {
        $data = $request->validate([
            'description' => 'required',
           'startDate' => 'required',
            'endDate' => 'required',
        ]);

        $task = Task::find($key);
        if($task) {
            $task->description = $data['description'];
            $task->startDate = $data['startDate'];
            $task->endDate = $data['endDate'];
            $task->update();

            return redirect('manageTask')->with('status', 'Updated!');
        }
        return redirect()->back()->with('status', 'Error!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        session()->forget('id');
        session()->forget('name');
        return redirect('/');
    }
}
