<?php

namespace App\Http\Controllers;

use App\Models\user;
use App\Models\emailverfication;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    // Example VerificationController.php
    public function verify($token)
    {
        $user = user::where('verification_token', $token)->first();
        $verified=emailverfication::where('user_id',$user->id)->first();
        if ($user) {

            $date= $user->verified_at=Carbon::now();
            if ($date<$verified->expired_at){
                $user->update(['status' => true,'verified_at'=>$date]);
                $verified->update(['is_verified'=>true]);
                return view('verificationSucess');
            }
            return redirect('register');
        } else {
            return redirect('register');
        }
    }


}
