<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class emailverfication extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id","is_verified","token","expired_at"];
    public function user()
    {
        return $this->belongsTo(user::class,"user_id","id");
    }
}
