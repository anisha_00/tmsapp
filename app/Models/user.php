<?php

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\emailverfication;
class user extends Authenticatable implements MustVerifyEmail
{
    use HasFactory,Notifiable;

    protected $fillable = [
        "name","email","password","phoneNo","status","verification_token","verified_at"];
    public function emailverfication(){
        return $this->hasOne(emailverfication::class,"user_id","id");
    }
    public function Task(){
        return $this->hasMany(Task::class,"user_id","id");
    }

}
