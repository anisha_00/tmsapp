<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task Management System</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="resources/js/jquery-3.5.1.js"></script></head>
<body>

<div class="row">
    <div class="col-md-4 m-auto" id="homepage">
        <center><h3>Choose your login</h3> </center>
        <a href="\login"class="btn btn-success">USER LOGIN</a>
        <a href="\register"class="btn btn-success">USER REGISTRATION</a>
        <a href="\admin"class="btn btn-success">ADMIN LOGIN</a>

    </div>
</div>

</body>
</html>
