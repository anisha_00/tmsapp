@extends('admin.adminDashboard')
@section('content')
    <script>
        var msg = '{{Session::get('status')}}';
        var exist = '{{Session::has('status')}}';
        if(exist){
            alert(msg);
        }
    </script>
 <div style="text-align: center">
    <h1 >
        Welcome to Admin dashboard
    </h1>
     <ul class="list-group">
         <li class="list-group-item">Hello {{Session::get('name')}}</li>
         <li class="list-group-item">Admin can edit create and delete task</li>
         <li class="list-group-item">Admin will select a user to assign a task</li>
         <li class="list-group-item">Admin should keep up with the tasks status!!</li>
     </ul>
 </div>
@endsection
