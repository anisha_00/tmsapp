@extends('admin.adminDashboard')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Create Task </title>
</head>
<script>
    var msg = '{{Session::get('status')}}';
    var exist = '{{Session::has('status')}}';
    if(exist){
        alert(msg);
    }
</script>
<body>
<div style=" display: grid;
    justify-content: center;">
    <h1>Create a new Task </h1>
<div class="row">
    <div class="col-md-6">
        <form action="createTask" method="post">
            @csrf <!-- {{ csrf_field() }} -->
            <div class="form-group">
                <label>Select User :</label>
                <select class="form-control" name="email" id="email">
                    <option>-Select-</option>
                    @foreach($users as $user)
                        <option value="{{ $user->email }}">{{ $user->email }}</option>
                    @endforeach
                </select>
                @error('email')
                <div class="text-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label>  Description :</label></br>
                <textarea class="from-control" rows="3" cols="80" name="description" id="description" placeholder="Mention the task" ></textarea>
                @error('description')
                <div class="text-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label>  Start Date : </label>
                <input type="date" class="form-control" name="startDate" id="startDate">
                @error('startDate')
                <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>  End Date : </label>
                <input type="date" class="form-control" name="endDate" id="endDate">
                @error('endDate')
                <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Create</button>

        </form>

    </div>



</div>
</div>
</body>
</html>
@endsection
