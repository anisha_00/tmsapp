@extends('admin.adminDashboard')
@section('content')
<html lang="en">
<script>
    var msg = '{{Session::get('status')}}';
    var exist = '{{Session::has('status')}}';
    if(exist){
        alert(msg);
    }
</script>
<body>

<h3>Total assigned task list</h3><br>
<table class="table">
    <tr>
        <th>Number</th>
        <th>Task ID</th>
        <th>Description</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Status</th>
        <th>Action</th>
    </tr>

    @foreach($tasks as $task)
        <tr>
        <td>{{$task->id}}</td>
        <td>{{$task->user_id}}</td>
        <td>{{$task->description}}</td>
        <td>{{$task->startDate}}</td>
        <td>{{$task->endDate}}</td>
        <td>{{$task->status}}</td>
        <td><a href="{{route('editTask',['key' => $task->id])}}">Update</a> <a href="{{route('deleteTask',['key' => $task->id])}}">Delete</a></td>
        </tr>
    @endforeach
</table>


</body>
</html>
@endsection
