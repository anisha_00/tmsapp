@extends('admin.adminDashboard')
@section('content')


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update your task</title>

</head>
</head>
<body>
<div style=" display: grid;
    justify-content: center;">
<div class="row">
    <div class="col-md-4 " ><br>
        <h3>Edit the task</h3>
            </div>
    <script>
        var msg = '{{Session::get('status')}}';
        var exist = '{{Session::has('status')}}';
        if(exist){
            alert(msg);
        }
    </script>
    <form action="{{route('editTask',['key' => $key])}}" method="post">
        @csrf <!-- {{ csrf_field() }} -->
            <div class="form-group">
                <label>Description:</label>
                <textarea class="form-control" rows="3" cols="80" name="description" id="description" ></textarea>
                @error('description')
                <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Start Date:</label>
                <input type="date" class="form-control" name="startDate" value="startDate">
                @error('startDate')
                <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>End Date:</label>
                <input type="date" class="form-control" name="endDate" value="endDate">
                @error('endDate')
                <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
           <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
</div>
</div>
</body>
</html
@endsection
