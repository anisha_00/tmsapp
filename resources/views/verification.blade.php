
@component('mail::message')
    <h1>Greetings</h1>
    @component('mail::button', ['url' => url('/verify/'.$user->verification_token)])
        Verify Email Address
    @endcomponent
@endcomponent
