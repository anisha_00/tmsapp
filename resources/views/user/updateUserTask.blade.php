@extends('user.userDashboard')
@section('content')
    <script>
        var msg = '{{Session::get('status')}}';
        var exist = '{{Session::has('status')}}';
        if(exist){
            alert(msg);
        }
    </script>
<div class="row">
    <div class="col-md-4 m-auto"><br>
        <h3>Update the task</h3>
        <form action="{{route('updateUserTask',['key' => $key])}}" method="post">
            @csrf <!-- {{ csrf_field() }} -->

            <div class="form-group">
                <input name="status" class="form-control" id="status" name="status" placeholder="Input your status">

                </input>
                @error('status')
                <div class="text-danger">{{$message}}</div>
                @enderror
            </div>
            <input type="submit" class="btn btn-success" name="update_task" value="Update" >
        </form>

    </div>
</div>
@endsection
