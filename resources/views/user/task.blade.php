@extends('user.userDashboard')
@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <script>
        var msg = '{{Session::get('status')}}';
        var exist = '{{Session::has('status')}}';
        if(exist){
            alert(msg);
        }
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width= , initial-scale=1.0">
    <title>TMS task</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

</head>
<body>
<center><h3>Tasks for {{Session::get('name')}}</h3></center>

<table class="table">
    <tr>
        <th>S.No</th>
        <th>Task ID</th>
        <th>Description</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Status</th>
        <th>Action</th>
    </tr>

    @foreach($tasks as $task)
        <tr>
            <td>{{$task->id}}</td>
            <td>{{$task->user_id}}</td>
            <td>{{$task->description}}</td>
            <td>{{$task->startDate}}</td>
            <td>{{$task->endDate}}</td>
            <td>{{$task->status}}</td>
            <td><a href="{{route('updateUserTask',['key' => $task->id])}}">Update</a> </td>
        </tr>
    @endforeach

</table>
</body>
</html>
@endsection
