@extends('user.userDashboard')
@section('content')
    <script>
        var msg = '{{Session::get('status')}}';
        var exist = '{{Session::has('status')}}';
        if(exist){
            alert(msg);
        }
    </script>
    <div style="text-align: center">
        <h1 >
            Welcome to User dashboard
        </h1>
        <ul class="list-group">
            <li class="list-group-item">Hello {{Session::get('name')}}</li>
            <li class="list-group-item">You can only view task</li>
            <li class="list-group-item">You can update your tasks status</li>
            <li class="list-group-item">Do checkout all your assigned tasks!!</li>
        </ul>
    </div>
@endsection
